<?php

use CommonsBooking\Helper\Helper;
use CommonsBooking\Messages\BookingMessage;

add_shortcode("friedablock2", "friedablock2_shortcode");

function friedablock2_shortcode()
{
    $html = '';
    $html .= '<h1>Tool für Buchungssperren (friedablock)</h1>';

    // Stop if user is not a least contributor (Mitarbeiter) (contributors and higher can 'edit_posts')
    if (!is_user_logged_in()) {
        $redirect_url = esc_url($_SERVER['REQUEST_URI']);
        $html .= '<h3>Bitte einloggen</h3>';
        $html .=("<script>window.location.href = '" . wp_login_url($redirect_url) . "';</script>");
        return $html;
    } else {
        if (!current_user_can('edit_posts')) {
            // User is logged in but cannot edit posts, display message
            $html .= '<h3>Zugang nur ab Mitarbeiter-Rolle (eng. contributor)</h3>';
            return $html;
        }
    }

    // inject CSS to hide header (logo, menu, slogan) of webpage when rendering page with shortcode
    $html .= <<<EOD
    <script>;
    jQuery(document).ready(function ($) {
        $('header').hide();
        $('main').css('padding', '1em');
        });
    </script>
    EOD;

    // Stop if user is not a least contributor (Mitarbeiter) (contributors and higher can 'edit_posts')
    if (!current_user_can('edit_posts')) {
        $html .= '<h3>Zugang nur ab Mitarbeiter-Rolle (eng. contributor)</h3>';
        return $html;
    }

    $sanitized_date_start = filter_input(INPUT_POST, 'date_start', FILTER_SANITIZE_STRING);
    $sanitized_date_end = filter_input(INPUT_POST, 'date_end', FILTER_SANITIZE_STRING);
    $sanitized_public_comment = filter_input(INPUT_POST, 'public_comment', FILTER_SANITIZE_STRING);
    $sanitized_lastenrad_ids = filter_input(INPUT_POST, 'lastenrad_id', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY) ?? array();

    $html .= <<<EOD
    <form id="blockform" method="post">
    Entweder jetzt bestehende <input type="submit"  name="list" value="Vorbuchungen anzeigen"> oder unten neue Buchungssperre eingeben.
    <hr>
    <h3>Neue Buchungssperre eingeben</h3>
    Lastenrad-Auswahl:<br>
    EOD;

    $cb_item_posts = get_posts(array(
        'post_type' => 'cb_item',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    ));

    // Check if there are any posts
    if ($cb_item_posts) {
        $html .= '<fieldset>';
        foreach ($cb_item_posts as $cb_item_post) {
            $item_id = $cb_item_post->ID;
            $post_title = get_the_title($cb_item_post);

            $ischecked = ""; 
            foreach($sanitized_lastenrad_ids as $tmp) {
                if($tmp == $item_id ) {
                    $ischecked = " checked=\"checked\"";
                    break;
                }
            }

            $html .= <<<EOD
            <label>
            <input type='checkbox' name='lastenrad_id[]' value='$item_id'$ischecked>
            $post_title <br>
            </label>
            EOD;
        }
        $html .= '</fieldset>';

        $escaped_public_comment = esc_html($sanitized_public_comment);

        $html .= <<<EOD
        <p><label>Startdatum:<br><input type='date' name='date_start' value='$sanitized_date_start'></label><br><label>Enddatum:<br><input type='date' name='date_end' value='$sanitized_date_end'></label></p>
        <p><label>Buchungskommentar:<br><input type='text' name='public_comment' value='$escaped_public_comment'><br></label></p><p>
        <input type='submit' name='submit' value='Buchungssperre anlegen'> </p>
        EOD;

    } else {
        $html .= 'Keine Lastenräder gefunden.<br>';
    }
    $html .= '</form>';

    $listShown = check_form_list_request($html);
    if ($listShown) return $html;

    // was a form POSTed? otherwise return
    if(!isset($_POST['submit'])) {
        return $html;
    }

    $html .= "<h3>Prüfung der Eingaben</h3>";

    // Create DateTime objects with explicit GMT timezone
    $timezone = new DateTimeZone('GMT');
    $start_date_time = DateTime::createFromFormat('Y-m-d', $sanitized_date_start, $timezone);
    $end_date_time = DateTime::createFromFormat('Y-m-d', $sanitized_date_end, $timezone);
    $today_date_time = new DateTime('now', $timezone);

    // Check if the date formats are valid
    if (!$start_date_time || !$end_date_time) {
        $html .= 'Startdatum und Enddatum fehlen.<br>';
        return $html;
    }

    $today_date_time->setTime(0, 0, 0);
    $start_date_time->setTime(0, 0, 0);
    $end_date_time->setTime(23, 59, 00);

    if ($start_date_time < $today_date_time) {
        $html .= 'Startdatum muss heute oder später sein.<br>';
        return $html;
    }

    if ($end_date_time < $start_date_time) {
        $html .= 'Enddatum muss gleich dem Startdatum oder später sein.<br>';
        return $html;
    }

    if (strlen($sanitized_public_comment)<3) {
        $html .= 'Ein Buchungskommentar ist Pflicht.<br>';
        return $html;
    }

    if (count($sanitized_lastenrad_ids) == 0) {
        $html .= 'Mindestens ein Lastenrad muss ausgewählt werden.<br>';
        return $html;
    }

    check_booking_query($html, $start_date_time, $end_date_time, $sanitized_public_comment, $sanitized_lastenrad_ids);

    return $html;

}

function check_booking_query(&$html, $start_date_time, $end_date_time, $public_comment, $lastenrad_ids) {

    // First check for each cb_item if booking can be performed (just check, don't book)
    $ok = true;
    foreach($lastenrad_ids as $lastenrad_id_dirty) {
        $lastenrad_id = intval($lastenrad_id_dirty);
        $ok &= performBooking($html, true, $lastenrad_id, $start_date_time, $end_date_time, $public_comment);
    }

    // If checks were all succesful, perform the bookings
    foreach($lastenrad_ids as $lastenrad_id_dirty) {
        if(!$ok) break;
        $lastenrad_id = intval($lastenrad_id_dirty);
        $ok &= performBooking($html, false, $lastenrad_id, $start_date_time, $end_date_time, $public_comment);
    }

    if ($ok) {
        $html .= 'Erfolgreich abgeschlossen.<br>';
    } else {
        $html .= 'Fehler sind aufgetreten.<br>';
    }

}

/* Create a cb_booking for one cb_itme for the given start end end time. */
function performBooking(&$html, $justCheck, $lastenrad_id, $start_date_time, $end_date_time, $public_comment) {

    $unix_time_start = $start_date_time->getTimestamp();
    $unix_time_end = $end_date_time->getTimestamp();
    $startTime_human = $start_date_time->format('H:i');
    $endTime_human = $end_date_time->format('H:i');

    if (get_post_type($lastenrad_id) !== "cb_item") {
        $html .= "Lastenrad-id " . $lastenrad_id . " ungültig.<br>";
        return false;
    }

    $bookableTimeframes = getBookableTimeframes($lastenrad_id, $unix_time_start, $unix_time_end);

    $showBookingCode = true;

    if (count($bookableTimeframes) == 0) {
        $html .= "Für lastenrad-id " . $lastenrad_id . " keine Zeitrahmen gefunden<br>";
        return false;
    } else {
        // the queried time interval fits in ONE or MORE bookable timeframes
        $first_iteration = true;
        foreach ($bookableTimeframes as $cb_timeframe_post) {
            $newlocationId = get_post_meta($cb_timeframe_post->ID, 'location-id', true);
            if (get_post_meta($cb_timeframe_post->ID, 'show-booking-codes', true) !== "on") {
                $showBookingCode = false;
            }
            if ($first_iteration) {
                $locationId = $newlocationId;
                $first_iteration = false;
            } else {
                if ($newlocationId != $locationId) {
                    $html .= "Für lastenrad-id " . $lastenrad_id . " mehr als 1 Zeitrahmen gefunden und sie stimmen bzgl. Verleihstation nicht überein!<br>";
                    return false;
                }
                $showBookingCode = false;
            }
        }
    }

    if (get_post_type($locationId) !== "cb_location") {
        $html .= "Location-id " . $locationId . " ungültig!<br>";
        return false;
    }

    // Check if there are previous (conflicting) bookings
    $conflicting_cb_booking_posts = get_posts(array(
        'post_type' => 'cb_booking',
        'posts_per_page' => -1,
        'post_status' => array('publish', 'confirmed'),
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'item-id',
                'value' => $lastenrad_id,
                'compare' => '='
            ),
            array(
                'key' => 'type',
                'value' => 6, // Timeframe::BOOKING_ID
                'compare' => '='
            ),
            array(
                'relation' => 'OR',
                array(
                    'relation' => 'AND',
                    array(
                        'key' => 'repetition-start',
                        'value' => $unix_time_start,
                        'compare' => '<='
                    ),
                    array(
                        'key' => 'repetition-end',
                        'value' => $unix_time_start,
                        'compare' => '>='
                    ),
                ),
                array(
                    'relation' => 'AND',
                    array(
                        'key' => 'repetition-start',
                        'value' => $unix_time_end,
                        'compare' => '<='
                    ),
                    array(
                        'key' => 'repetition-end',
                        'value' => $unix_time_end,
                        'compare' => '>='
                    ),
                ),
            ),
        ),
    ));

    if ($conflicting_cb_booking_posts) {
        foreach ($conflicting_cb_booking_posts as $cb_booking_post) {
            $booking_id = $cb_booking_post->ID;
            $html .= "Eine Buchung ($booking_id) ist schon vorhanden.<br>";
            return false;
        }
    }

    if (!$justCheck) {
        $postarray  = [
            'post_title'  => 'Vorbuchung',
            'post_name'  => Helper::generateRandomString(),
            'post_type'   => "cb_booking",
            'post_status' => 'confirmed',
            'post_author' => get_current_user_id()
        ];
        $bookingId = wp_insert_post( $postarray );
        
        if (!$bookingId) {
            $html .= "Fehler beim Buchen (wp_insert_post)<br>";
            return false;
        }

        update_post_meta( $bookingId, 'type', 6 ); // Timeframe::BOOKING_ID
        update_post_meta( $bookingId, 'start-time', $startTime_human );
        update_post_meta( $bookingId, 'end-time', $endTime_human );
        update_post_meta( $bookingId, 'location-id', $locationId );
        update_post_meta( $bookingId, 'item-id', $lastenrad_id );
        update_post_meta( $bookingId, 'full-day', 'on' );
        update_post_meta( $bookingId, 'repetition-start', $unix_time_start );
        update_post_meta( $bookingId, 'repetition-end', $unix_time_end );
        update_post_meta( $bookingId, 'comment', $public_comment );
        update_post_meta( $bookingId, 'booking-type', 'friedablock' );

        if ($showBookingCode) {
            $code = getBookingCode($lastenrad_id, $locationId, $start_date_time->format('Y-m-d'));
            update_post_meta( $bookingId, 'show-booking-codes', 'on' );
            update_post_meta( $bookingId, '_cb_bookingcode', $code );
        }

        // to update Commonsbooking-Cache, it is necessary to call wp_update_post() to trigger certain hooks
        // even if post_status is not changed. Changing post_status additionally triggers a mail to the booking user    
        $postarray['ID'] = $bookingId;
        $booking_msg = new BookingMessage($bookingId, "confirmed");
        $booking_msg->triggerMail();
        if (!wp_update_post($postarray)) {
            $html .= "Fehler beim Buchen (wp_update_post)<br>";
            return false;
        }
    }

    return true;

}

function getBookableTimeframes($lastenrad_id, $booking_unix_time_start, $booking_unix_time_end) {

    $cb_timeframe_posts = get_posts(array('post_type' => 'cb_timeframe',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'key' => 'item-id',
                'value' => $lastenrad_id,
                'compare' => '='
            ),
            array(
                'key' => 'type',
                'value' => 2, // Timeframe::BOOKABLE_ID
                'compare' => '='
            ),
            array(
                'key' => 'repetition-start',
                'value' => $booking_unix_time_start,
                'compare' => '<='
            ),
            array(
                'key' => 'repetition-end',
                'value' => $booking_unix_time_end,
                'compare' => '>='
            ),
        ),
    ));

    return $cb_timeframe_posts;

}


function getBookingCode(int $itemId, int $locationId, string $date) {

    global $wpdb;
    $table_name = $wpdb->prefix . 'cb_bookingcodes';

    $sql = $wpdb->prepare(
        "SELECT * FROM $table_name
        WHERE 
            item = %s AND 
            location = %s AND 
            date = %s
        ORDER BY item ASC ,date ASC",
        $itemId,
        $locationId,
        $date
    );
    $bookingCodes = $wpdb->get_results($sql);

    if (count($bookingCodes) == 1) {
        $code = $bookingCodes[0]->code;
        return $code;
    } else {
        return false;
    }

}


function check_form_list_request(&$html) {

    // was list button POSTed? otherwise return
    if(!isset($_POST['list'])) {
        return false;
    }

    $block_posts = get_posts(array('post_type' => 'cb_booking',
        'posts_per_page' => -1,
        'post_status' => array('publish', 'confirmed'),
        'meta_query' => array(
            array(
                'key' => 'booking-type',
                'value' => 'friedablock',
                'compare' => '='
            ),
        )
    ));

    $html .= <<<EOD
        <h2>Aufzählung bestehender Buchungssperren</h2>
        <table class='bookinglist'>
        <tr><th>id</th><th>Startdatum</th><th>Enddatum</th><th>Lastenrad</th><th>Nutzer</th><th>Kommentar</th><th>Code</th></tr>
        EOD;

    foreach($block_posts as $post) {
        $id = $post->ID;

        $user = get_userdata($post->post_author);
        $username = $user->user_login;

        $itemId = get_post_meta($id, 'item-id', true);
        $itemPost = get_post($itemId);
        $item_name = $itemPost->post_title;

        $bookingcode = get_post_meta($id, '_cb_bookingcode', true);
        $unix_time_start = get_post_meta($id, 'repetition-start', true);
        $unix_time_end = get_post_meta($id, 'repetition-end', true);

        $start_date_time = new DateTime("@$unix_time_start");
        $start_date_time->setTimeZone(new DateTimeZone('GMT'));
        $end_date_time = new DateTime("@$unix_time_end");
        $end_date_time->setTimeZone(new DateTimeZone('GMT'));
        $date1= $start_date_time->format('Y-m-d');
        $date2= $end_date_time->format('Y-m-d');

        $comment_wo_linebreaks = get_post_meta($id, 'comment', true);
        $html .= <<<EOD
            <tr><td>$id</td><td>$date1</td><td>$date2</td><td>$item_name</td><td>$username</td><td>$comment_wo_linebreaks</td><td>$bookingcode</td></tr>
            EOD;
    }
    $html .=("</table>");
    return true;
}

?>