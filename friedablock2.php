<?php
/**
 * Plugin Name: Friedablock2
 * Description: Blocken/Vorbuchen für CommonsBooking2. Platziere shortcode [friedablock2] auf sonst leerer Seite z.B. server.com/friedablock. Zugriff ab contributor=Mitarbeiter.
 * Author: Nils Larsen
 * Author URI: https://www.friedafriedrich.de
 */

require_once "friedablock2_endpoint_shortcode.php";

function friedablock2_enqueue_custom_styles() {
    wp_enqueue_style('friedablock2-styles', plugin_dir_url(__FILE__) . 'friedablock2.css');
}
add_action('wp_enqueue_scripts', 'friedablock2_enqueue_custom_styles');

?>